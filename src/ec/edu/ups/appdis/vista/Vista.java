package ec.edu.ups.appdis.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Vista extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtCantidad;
	private JTextField txtPrecio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista frame = new Vista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Vista() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cantidad:");
		lblNewLabel.setBounds(25, 89, 75, 22);
		contentPane.add(lblNewLabel);
		
		JLabel lblNombreProducto = new JLabel("Nombre Producto:");
		lblNombreProducto.setBounds(25, 56, 127, 22);
		contentPane.add(lblNombreProducto);
		
		JLabel lblPrecio = new JLabel("Precio:");
		lblPrecio.setBounds(25, 134, 75, 22);
		contentPane.add(lblPrecio);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(162, 57, 119, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtCantidad = new JTextField();
		txtCantidad.setColumns(10);
		txtCantidad.setBounds(108, 90, 119, 20);
		contentPane.add(txtCantidad);
		
		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(108, 135, 119, 20);
		contentPane.add(txtPrecio);
	}
}
