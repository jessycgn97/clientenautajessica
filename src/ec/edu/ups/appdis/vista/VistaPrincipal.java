package ec.edu.ups.appdis.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import java.awt.Color;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField txtCedula;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtTipo;
	

    private int codAux;
    private JTextField txtCedulaCon;
    private JTextField txtNombreCon;
    private JTextField txtCedulaBuscar;
    private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaPrincipal frame = new VistaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public VistaPrincipal() throws Exception {
        codAux = -500;
        
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 409);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(224, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 5, 424, 354);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Crear Contacto", null, panel, null);
		panel.setLayout(null);
		
		txtCedula = new JTextField();
		txtCedula.setBounds(83, 54, 113, 20);
		panel.add(txtCedula);
		txtCedula.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Cedula:");
		lblNewLabel.setBounds(31, 57, 42, 14);
		lblNewLabel.setLabelFor(this);
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		panel.add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setVerticalAlignment(SwingConstants.TOP);
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setBounds(31, 88, 48, 14);
		panel.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		txtNombre.setBounds(83, 85, 113, 20);
		panel.add(txtNombre);
		
		JLabel lblNewLabel_1 = new JLabel("CREAR CONTACTO");
		lblNewLabel_1.setForeground(new Color(255, 0, 0));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1.setBounds(113, 8, 182, 20);
		panel.add(lblNewLabel_1);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setVerticalAlignment(SwingConstants.TOP);
		lblTipo.setHorizontalAlignment(SwingConstants.LEFT);
		lblTipo.setBounds(31, 215, 48, 14);
		panel.add(lblTipo);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setVerticalAlignment(SwingConstants.TOP);
		lblTelefono.setHorizontalAlignment(SwingConstants.LEFT);
		lblTelefono.setBounds(27, 179, 52, 14);
		panel.add(lblTelefono);
		
		txtTelefono = new JTextField();
		txtTelefono.setColumns(10);
		txtTelefono.setBounds(93, 176, 113, 20);
		panel.add(txtTelefono);
		
		txtTipo = new JTextField();
		txtTipo.setColumns(10);
		txtTipo.setBounds(93, 212, 113, 20);
		panel.add(txtTipo);
		
		JLabel lblNewLabel_1_1 = new JLabel("TELEFONO\r\n");
		lblNewLabel_1_1.setForeground(Color.RED);
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_1_1.setBounds(149, 145, 107, 20);
		panel.add(lblNewLabel_1_1);
		
		JButton btnNewButton = new JButton("Añadir Telefono");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		
		btnNewButton.setBounds(230, 206, 133, 23);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("GUARDAR\r\n");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		btnNewButton_1.setBounds(147, 260, 109, 23);
		panel.add(btnNewButton_1);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Listar Contacto", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("LISTAR CONTACTO");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblNewLabel_2.setForeground(new Color(255, 0, 0));
		lblNewLabel_2.setBounds(93, 11, 212, 22);
		panel_1.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Cedula:");
		lblNewLabel_3.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_3.setBounds(35, 102, 42, 14);
		panel_1.add(lblNewLabel_3);
		
		txtCedulaCon = new JTextField();
		txtCedulaCon.setColumns(10);
		txtCedulaCon.setBounds(87, 99, 113, 20);
		panel_1.add(txtCedulaCon);
		
		JLabel lblNombre_1 = new JLabel("Nombre:");
		lblNombre_1.setVerticalAlignment(SwingConstants.TOP);
		lblNombre_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre_1.setBounds(35, 133, 48, 14);
		panel_1.add(lblNombre_1);
		
		txtNombreCon = new JTextField();
		txtNombreCon.setColumns(10);
		txtNombreCon.setBounds(87, 130, 113, 20);
		panel_1.add(txtNombreCon);
		
		JLabel lblNewLabel_3_1 = new JLabel("Cedula del contacto a buscar:");
		lblNewLabel_3_1.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_3_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel_3_1.setBounds(35, 60, 147, 14);
		panel_1.add(lblNewLabel_3_1);
		
		txtCedulaBuscar = new JTextField();
		txtCedulaBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		txtCedulaBuscar.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
		            
		            
		        }
			}
			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});
		txtCedulaBuscar.setColumns(3);
		txtCedulaBuscar.setBounds(192, 57, 113, 20);
		panel_1.add(txtCedulaBuscar);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
			},
			new String[] {
				"Codigo", "Numero", "Tipo"
			}
		));
		table.setBounds(35, 172, 356, 117);
		panel_1.add(table);
	}   
    
   
}
