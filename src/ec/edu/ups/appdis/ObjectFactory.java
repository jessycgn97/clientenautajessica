
package ec.edu.ups.appdis;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.edu.ups.appdis package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListaProducto_QNAME = new QName("http://services.appdis.ups.edu.ec/", "listaProducto");
    private final static QName _ListaProductoResponse_QNAME = new QName("http://services.appdis.ups.edu.ec/", "listaProductoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.edu.ups.appdis
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListaProductoResponse }
     * 
     */
    public ListaProductoResponse createListaProductoResponse() {
        return new ListaProductoResponse();
    }

    /**
     * Create an instance of {@link ListaProducto }
     * 
     */
    public ListaProducto createListaProducto() {
        return new ListaProducto();
    }

    /**
     * Create an instance of {@link Producto }
     * 
     */
    public Producto createProducto() {
        return new Producto();
    }

    /**
     * Create an instance of {@link Inventario }
     * 
     */
    public Inventario createInventario() {
        return new Inventario();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaProducto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.appdis.ups.edu.ec/", name = "listaProducto")
    public JAXBElement<ListaProducto> createListaProducto(ListaProducto value) {
        return new JAXBElement<ListaProducto>(_ListaProducto_QNAME, ListaProducto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListaProductoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.appdis.ups.edu.ec/", name = "listaProductoResponse")
    public JAXBElement<ListaProductoResponse> createListaProductoResponse(ListaProductoResponse value) {
        return new JAXBElement<ListaProductoResponse>(_ListaProductoResponse_QNAME, ListaProductoResponse.class, null, value);
    }

}
