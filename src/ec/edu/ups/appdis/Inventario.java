
package ec.edu.ups.appdis;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para inventario complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="inventario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_inventario" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="listaProducto" type="{http://services.appdis.ups.edu.ec/}producto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="stock" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inventario", propOrder = {
    "idInventario",
    "listaProducto",
    "stock"
})
public class Inventario {

    @XmlElement(name = "id_inventario")
    protected int idInventario;
    @XmlElement(nillable = true)
    protected List<Producto> listaProducto;
    protected int stock;

    /**
     * Obtiene el valor de la propiedad idInventario.
     * 
     */
    public int getIdInventario() {
        return idInventario;
    }

    /**
     * Define el valor de la propiedad idInventario.
     * 
     */
    public void setIdInventario(int value) {
        this.idInventario = value;
    }

    /**
     * Gets the value of the listaProducto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaProducto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaProducto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Producto }
     * 
     * 
     */
    public List<Producto> getListaProducto() {
        if (listaProducto == null) {
            listaProducto = new ArrayList<Producto>();
        }
        return this.listaProducto;
    }

    /**
     * Obtiene el valor de la propiedad stock.
     * 
     */
    public int getStock() {
        return stock;
    }

    /**
     * Define el valor de la propiedad stock.
     * 
     */
    public void setStock(int value) {
        this.stock = value;
    }

}
